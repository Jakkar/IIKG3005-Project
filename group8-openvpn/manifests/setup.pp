# Setup class for installing and configuring openvpn and the firewall
class openvpn::setup {
  require openvpn::pki

  Class['openvpn::pki'] -> Class['openvpn::setup']

  package { ['openvpn', 'ufw']:
    ensure => present,
  }

  exec { 'ufw-rules':
    command => 'ufw allow 1194/udp; ufw allow OpenSSH; ufw disable; ufw enable',
    path    => '/usr/sbin/',
    require => Package['openvpn']
  }

  exec { 'ta-key':
    command =>'openvpn --genkey secret /etc/openvpn/ta.key',
    path    => '/usr/sbin/',
    cwd     => "/etc/easyrsa/${$openvpn::pki}/",
    creates => '/etc/openvpn/ta.key',
    require => Package['openvpn']
  }

  file { '/etc/openvpn/ca.crt':
    ensure => present,
    source => "/etc/easyrsa/${$openvpn::pki}/ca.crt"
  }

  file { '/etc/openvpn/dh.pem':
    ensure => present,
    source => "/etc/easyrsa/${$openvpn::pki}/dh.pem"
  }

  file { '/etc/openvpn/server.key':
    ensure => present,
    source => "/etc/easyrsa/${$openvpn::pki}/private/${$openvpn::server}.key"
  }

  file { '/etc/openvpn/server.crt':
    ensure => present,
    source => "/etc/easyrsa/${$openvpn::pki}/issued/${$openvpn::server}.crt"
  }

  service { 'openvpn-server':
    ensure  => 'running',
    enable  => true,
    name    => 'openvpn@server',
    require => [Exec['ta-key'], File['/etc/openvpn/ca.crt']]
  }

  # oml bad practice but works for our specific setup

  file { '/etc/ufw/before.rules':
    source => 'puppet:///modules/openvpn/before.rules',
    before => Service['openvpn-server']
  }

  file { '/etc/default/ufw':
    source => 'puppet:///modules/openvpn/default',
    before => Service['openvpn-server']
  }

  file { '/etc/sysctl.conf':
    source => 'puppet:///modules/openvpn/sysctl.conf',
    before => Service['openvpn-server']
  }

  file { '/etc/openvpn/server.conf':
    source => 'puppet:///modules/openvpn/server.conf',
    before => Service['openvpn-server']
  }
}
