# PKI class responsible for using the easyrsa module to create a private key infrastructure 
# and use it to generate and sign keys and certificates
class openvpn::pki {
  class { 'easyrsa':
    pkis     => {
      $openvpn::pki => {},
    },
    cas      => {
      $openvpn::pki => {
        key => {
          algo       => ec,
          size       => 64,
          valid_days => 30,
        },
      },
    },
    dhparams => {
      $openvpn::pki => {
        key_size => 2048,
      },
    },
    servers  => {
      $openvpn::server => {
        pki_name => $openvpn::pki,
        key      => {
          algo       => ec,
          size       => 64,
          valid_days => 30,
        },
      }
    },
    clients  => {
      $openvpn::clients => {
        pki_name => $openvpn::pki,
        key      => {
          algo       => ec,
          size       => 64,
          valid_days => 30,
        },
      }
    }
  }
}
