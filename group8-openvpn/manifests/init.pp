# The main class used for main parameters and running of subclasses
class openvpn (
  String $ip,
  String $pki,
  String $server,
  Array[String] $clients
) {
  include openvpn::pki
  include openvpn::setup
  include openvpn::clientsconfig

  Class['openvpn::pki'] ~> Class['openvpn::setup'] ~> Class['openvpn::clientsconfig']
}
