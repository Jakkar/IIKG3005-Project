# Class for generating client config files (.ovpn)
class openvpn::clientsconfig {
  $pki = $openvpn::pki;
  $ip = $openvpn::ip;

  $base_config = epp('openvpn/client.conf.epp', {'ip' => $ip});

  if (find_file('/etc/openvpn/ca.crt')) {
    $ca_cert = file('/etc/openvpn/ca.crt');
    $ta_key = file('/etc/openvpn/ta.key');

    $openvpn::clients.each |String $client| {
      $client_cert = file("/etc/easyrsa/${pki}/issued/${client}.crt");
      $client_key = file("/etc/easyrsa/${pki}/private/${client}.key");

      $content = "${base_config}\n
                  <ca>\n${ca_cert}\n</ca>\n
                  <cert>\n${client_cert}\n</cert>\n
                  <key>\n${client_key}\n</key>\n
                  <tls-auth>\n${ta_key}\n</tls-auth>";

      file { "/etc/easyrsa/${pki}/issued/${client}.ovpn":
        ensure  => present,
        content => $content
      }
    }
  }
}
