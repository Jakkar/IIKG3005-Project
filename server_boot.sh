#!/bin/bash
sudo su
cd /

logfile=/var/log/openstack_boot.log

echo "Starting openstack server boot script..." > $logfile

log () {
  echo $1 >> $logfile
}

log "Installing puppet"
# Installing puppet
wget https://apt.puppetlabs.com/puppet6-release-bionic.deb
dpkg -i puppet6-release-bionic.deb
apt-get update
apt-get install puppet-agent
# add Puppet binaries to PATH:
echo 'export PATH=$PATH:/opt/puppetlabs/bin/' >> ~/.bashrc
# source .bashrc to apply:
source ~/.bashrc

log "Installing git"

# Download git
apt-get install --yes git
	
log "Cloning project repository"

# Get from git
git clone https://gitlab.com/Jakkar/IIKG3005-Project.git

log "Copying puppet files"

# Copying files
mkdir /etc/puppetlabs/code/environments/production/modules/openvpn
cp -r IIKG3005-Project/group8-openvpn/* /etc/puppetlabs/code/environments/production/modules/openvpn
cp IIKG3005-Project/site.pp /etc/puppetlabs/code/environments/production/manifests/

log "Installing EasyRSA Puppet module"

# Install modules
puppet module install rehan-easyrsa

for number in {1..4}
do
log "Running puppet apply $number"
puppet apply /etc/puppetlabs/code/environments/production/manifests/site.pp
done

log "Done"
