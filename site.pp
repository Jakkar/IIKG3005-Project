
node default {
  class { 'openvpn':
    ip      => '127.0.0.1',
    pki     => 'pki',
    server  => 'server',
    clients => ['client1', 'client2']    
  }
}
