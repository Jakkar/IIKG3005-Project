# IIKG3005 Infrastructure as Code - Project

## Group 8

- Even Bryhn Bøe
- Magnus Johan Gluppe
- Ruben Christoffer Hegland-Antonsen
- Jakob Frantzvåg Karlsmoen

## Puppet module usage

The puppet module is intended to be used on a fresh Debian 11 server, with Puppet version 6.25!

### Example manifest:
```
node default {
  class { 'openvpn':
    ip      => '127.0.0.1',
    pki     => 'pki',
    server  => 'server',
    clients => ['client1', 'client2']    
  }
}
```
- ip: the public ip of the server
- pki: the name of the public key infrastructure to use. 
    - if it does not exist, a new one will be created
    - if it exists, it will use existing keys to generate and sign certificates
- server: name of openvpn server keys/certificates/config
- clients: array of names of client keys/certificate/config

### Client configurations

The client configurations are placed in `/etc/easyrsa/{{pki}}/issued/`

If everything worked correctly, the configurations are complete and ready to be used on clients.

## Usage with SkyHigh

### Create stack

A stack can be created with `./create_stack {{name of stack}} {{key_name}}`

Example:
```
./create_stack vpn_stack my_key
```

After the stack is created **server_boot.sh** runs automatically on the server

### Connect to stack

To connect to the server create, use `./connect_to_stack {{path_to_key}} {{username}} {{vpn}} server_public_ip`
This command uses _get_output_value.py_ to get one of the output fields in _vpn_server.yaml_. 
The field in question here is _server_public_ip_, which is the public ip of the server created as part of the stack and is given in the output field of the stack. This will allways be **server_public_ip**, but in the case where you have several servers in the stack, this can be used to distinguish between them. 

Example:
```
./connect_to_stack my_key.pem debian vpn_stack server_public_ip
```

### Client Configs
See _Client configurations_ under _Puppet module usage_
