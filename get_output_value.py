import json
import sys

# Send in output of following command as first argument: openstack stack show vpn_test -f json --noindent -c outputs

def main():
    args = sys.argv[1:]
    if len(args) == 0:
        exit()
    inp = json.loads(args[0])
    if len(args) == 1:
        print(inp)
    else:
        for i in inp['outputs']:
            if i['output_key'] == args[1]:
                print(i['output_value'])

if __name__ == '__main__':
    main()
